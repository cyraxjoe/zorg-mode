(defconst day-in-seconds  (* 60 60 24))
(defconst zorg:log-dir  (expand-file-name "~/repos/org/log/"))
(defvar zorg:visiting-file (list))

(defun get-dow (date)
  (let ((days '(:sunday :monday :tuesday :wednesday :thursday :friday :saturday)))
	(nth (nth 6 (decode-time date)) days)))

(defun zorg:date-to-fname (&optional date delta-days)
  (unless date (setf date (current-time)))
  (unless delta-days (setf delta-days 0))
  (let* ((delta-days-in-seconds (* day-in-seconds (abs delta-days)))
		 (time-delta-func (if (< delta-days 0) #'time-subtract #'time-add))
		 (final-date (funcall time-delta-func date delta-days-in-seconds)))
	(concat zorg:log-dir (format-time-string "%Y-%m-%d.md" final-date))))

(defun zorg:prev-working-day-fname ()
  (let* ((today (current-time))
		 (delta (- (if (equal (get-dow today) :monday) 3 1))))
	(zorg:date-to-fname today delta)))

(defun zorg:next-working-day-fname ()
  (let* ((today (current-time))
		 (delta  (if (equal (get-dow today) :friday) 3 1)))
	(zorg:date-to-fname today delta)))


(defun zorg:open-today ()
  (interactive )
  (find-file (zorg:date-to-fname)))


(defun zorg:open-next-work-day (&optional find-func)
  (interactive)
  (unless find-func (setf find-func #'find-file))
  (funcall find-func (zorg:next-working-day-fname)))
	
(defun zorg:open-prev-work-day (&optional find-func)
  (interactive)
  (unless find-func (setf find-func #'find-file))
  (funcall find-func (zorg:prev-working-day-fname)))


(defun zorg:open-next-week (&optional extra-days find-func)
  (interactive "nExtra days: ")
  (unless extra-days (setf extra-days 0))
  (unless find-func (setf find-func #'find-file))
  (funcall find-func (zorg:date-to-fname nil (+ 7 extra-days))))

(defun zorg:start ()
  (interactive)
  (zorg:open-today)
  (split-window-horizontally)
  (zorg:open-prev-work-day #'find-file-other-window))

(defun zorg:insert-segment (&optional user)
  (interactive)
  (unless user (setf user "joel"))
  (insert (format "\n# %s\n\n---\n\n---\n" user))
  (previous-line) (previous-line))


(defun zorg:mark-task (&optional check-char)
  (unless check-char (setf check-char "x"))
  (let ((original-point (point))
		(checkbox (search-backward-regexp "\\[.\\]")))
	(when checkbox
	  (progn
		(goto-char (1+ checkbox))
		(delete-char 1)
		(insert check-char)
		(goto-char original-point)))))


(defun zorg:task-done ()
  (interactive)
  (zorg:mark-task "x"))

(defun zorg:task-wip ()
  (interactive)
  (zorg:mark-task "."))

(defun zorg:task-paused ()
  (interactive)
  (zorg:mark-task "p"))

(defun zorg:task-waiting ()
  (interactive)
  (zorg:mark-task "w"))

(defun zorg:task-clear ()
  (interactive)
  (zorg:mark-task " "))


(defun zorg:goto-file ()
  (interactive)
  (let* ((center (point))
		 (eol (search-forward-regexp "$"))
		 (init-pos (1+ (search-backward " ")))
		 (end-pos (progn (goto-char center)
						 (let ((eow (search-forward-regexp "[[:space:]]")))
						   (min eol eow)))))
	(progn 
	  (goto-char center)
	  (copy-region-as-kill init-pos end-pos)
	  (let* ((raw-filename (car kill-ring))
			 (filename  (if (string-prefix-p "log/" raw-filename)
							(file-name-nondirectory raw-filename)
						  raw-filename))
			 (bufferdir (file-name-directory buffer-file-name))
			 (basedir (if (string-suffix-p "/log/" bufferdir)
						  bufferdir
						(concat bufferdir "/log/")))
			 (fullpath (concat basedir filename)))
		(and (push fullpath zorg:visiting-file) (find-file fullpath))))))


(defun zorg:go-back ()
  (interactive)
  (and (pop zorg:visiting-file) (previous-buffer)))


;; keybinding
(defvar zorg-mode-map nil
  "Local key map to the zorg mode.")

(setq zorg-mode-map (make-sparse-keymap))

(define-key zorg-mode-map "\M-." #'zorg:goto-file)
(define-key zorg-mode-map "\M-," #'zorg:go-back)
(define-key zorg-mode-map "\M-\r" #'markdown-follow-thing-at-point)
(define-key zorg-mode-map "\C-x\M-i" #'zorg:insert-segment)
;; Ctrl-c +
(define-key mode-specific-map "x" #'zorg:task-done)
(define-key mode-specific-map "w" #'zorg:task-waiting)
(define-key mode-specific-map "p" #'zorg:task-paused)
(define-key mode-specific-map "." #'zorg:task-wip)
(define-key mode-specific-map "c" #'zorg:task-clear)


(define-minor-mode zorg-mode
  "Zorg minor mode for zorg task management."
  :init-value nil
  :lighter " zorg"
  :keymap 'zorg-mode-map)

(defun maybe-hook-to-markdown-mode ()
  (let ((bufferdir (file-name-directory buffer-file-name)))
	(if (or (string-suffix-p "org/" bufferdir)
			(string-suffix-p "org/log/" bufferdir))
		(zorg-mode))))

(add-hook 'markdown-mode-hook #'maybe-hook-to-markdown-mode)

(provide 'zorg)
